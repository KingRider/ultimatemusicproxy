#!/usr/bin/env python
# -*- coding: utf-8 -*-
global version
version = "1.0"
import sys
import os


from proxyfinder import *
global globalProxy
globalProxy = None
from proxyserver import *

def help():
    print "-----------------------------------------------------"
    print "Commands: "
    print "Q to exit"
    print "UK to use GB proxy list"
    print "US to use US proxy list"
    print "E to toggle only elite proxies"
    print "P to get new proxy"
    print "T to get and test new proxy"
    print "? to view proxy data"
    print "S to manually set proxy"
    print "-----------------------------------------------------"


def main(HandlerClass=ProxyRequestHandler, ServerClass=ThreadingHTTPServer, protocol="HTTP/1.1"):
    if sys.argv[1:] and sys.argv[1].isdigit():
        port = int(sys.argv[1])
    else:
        port = 3128
    server_address = ('', port)
    pf = ProxyFinder()
    if '--united-kingdom' in sys.argv:
    	pf.listUrl = UK_PROXY_LIST
    	print "Using UK proxy list"
    else:
    	print "Using US proxy list"
    if '--strict' in sys.argv:
    	pf.strict = True
    	print "Strict mode is now ON"
    else:
    	print "Strict mode is now OFF"
    global globalProxy
    if '--enable-spotify' in sys.argv:
    	if '--test-proxy' in sys.argv:
    		print "Looking for a tested proxy, this may take a while..."
    		globalProxy = pf.getTestedProxy()
    	else:
    		print "Looking for a proxy (no testing)..."
    		globalProxy = pf.getGoodProxy()
    	print "Found:",globalProxy[0],":",globalProxy[1]
    HandlerClass.protocol_version = protocol
    httpd = ServerClass(server_address, HandlerClass)

    sa = httpd.socket.getsockname()
    htth = threading.Thread(target=httpd.serve_forever)
    htth.daemon = True
    htth.start()
    print "Serving HTTP Proxy on", sa[0], "port", sa[1], "..."
    shouldContinueToConsole = True
    if '--execute' in sys.argv:
    	idx = sys.argv.index('--execute') + 1
    	if sys.argv[idx:]:
    		shouldContinueToConsole = False
    		os.system(sys.argv[idx])
    if not shouldContinueToConsole:
    	return
    if sys.argv[1:] and not sys.argv[1].isdigit():
    	print "Specify port as the second argument if you need to change it."
    if not '--enable-spotify' in sys.argv:
    	print "-----------------------------------------------------"
    	print "There is no HTTP Proxy set, which is required for Spotify to operate properly. Refer to the Proxy commands to set a proxy address. "
    	print "To get a proxy automatically, use --enable-spotify."
    	print "To use a UK proxy instead of an US proxy, add --united-kingdom. "
    	print "-----------------------------------------------------"
    help()
    sas = raw_input().lower()
    while not sas == 'q':
    	if sas == 'p':
    		print "Looking for proxy..."
    		globalProxy = pf.getGoodProxy()
    		print "Found:",globalProxy[0],":",globalProxy[1]
    	elif sas == 't':
    		print "Looking for proxies and testing them, this can take a while..."
    		globalProxy = pf.getTestedProxy()
    		print "Found:",globalProxy[0],":",globalProxy[1]
    	elif sas == 's':
    		uri = raw_input('HTTP Proxy Host: ')
    		pr = raw_input('HTTP Proxy Port: ')
    		globalProxy = [uri,pr]
    	elif sas == '?':
    		if globalProxy is not None:
    			print 'HTTP Proxy in use: ',globalProxy[0],':',globalProxy[1]
    		else:
    			print 'No HTTP Proxy in use. Use P, T or S to get one automatically or input manually.'
    	elif sas == 'h':
    		help()
    	elif sas == 'uk':
    		pf.listUrl = UK_PROXY_LIST
    		print "Switched to UK. Use P or T to get a new proxy from the selected country."
    	elif sas == 'us':
    		pf.listUrl = US_PROXY_LIST
    		print "Switched to US. Use P or T to get a new proxy from the selected country."
    	elif sas == 'e':
    		pf.strict = not pf.strict
    		print "Strict mode is now",("ON" if pf.strict else "OFF"),", use P or T to get a new proxy matching the setting."
    	sas = raw_input().lower()


if __name__ == '__main__':
    print ",--. ,--.,--.   ,--.,------.  "
    print "|  | |  ||   `.'   ||  .--. ' "
    print "|  | |  ||  |'.'|  ||  '--' | "
    print "'  '-'  '|  |   |  ||  | --'  "
    print " `-----' `--'   `--'`--'   "
    print ""
    print "Ultimate Music Proxy"
    print "by Genjitsu Gadget Lab"
    print ""
    main()
