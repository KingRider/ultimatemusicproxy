# UMP Quick Setup Guide (with Digitally Imported)

## On Windows

*Warning:* I've already had Python with PIP up and running so this is only correct to the point of how my memory keeps it :) refer to the official documentation in case you have any problems


### Step 1. Install Python 2.7 with PIP

You can get Python 2.7 on the [official website](https://www.python.org/downloads/), this should include PIP as well.

Run the installer and follow the instructions. The preferred installation directory is `C:\Python27` and it will be used in all further instructions. 

### Step 2. Set up required dependencies

So far the only dependency is BeautifulSoup. Install it with the following command: `C:\Python27\scripts\pip install beautifulsoup`

### Step 3. Download UMP

Use the download button on the left at Bitbucket to download a zip archive of the UMP repo. Unpack the UMP files (`UltimateProxy.py`, `proxyfinder.py` and `proxyserver.py`, the rest can be discarded) into an install directory of your choice (i.e. `C:\UMP`). 

### Step 4. Create a shortcut to UMP

If you've used the official Python installer, `.py` files should be associated with Python automatically. Right-click the `UltimateProxy.py` file and create a shortcut to it. Place it in a convenient location of your choice.

### If you've done it right...

Once you double-click the shortcut, a command prompt window should appear. Shortly after, the UMP logo will appear in it. Then it will show you the port it's working on and a list of commands allowed. Stop UMP by typing Q, then pressing Enter.

### Finishing up

Refer to your media player's user manual to set the streaming proxy to HTTP type on address `127.0.0.1` port `3128`. **It is strongly *not* recommended to set UMP as the system proxy because it is not that stable yet.** Start UMP from the shortcut you've created before. Open a Digitally Imported stream in your player you've just configured (refer to DI, i.e. [here](http://pub7.di.fm), for a list of streams). Instead of a restriction notice, you should hear the current broadcast of the station you've chosen. You're now set up with UMP as a DI.fm proxy, enjoy!

## On Mac

Macs usually come with Python preinstalled, but the rest is almost the same.

### Step 1. Install the dependencies

So far the only dependency is BeautifulSoup. Install it with the following command in Terminal: `pip install beautifulsoup`. Depending on your OS setup you might need administrative rights, in that case add `sudo` in front of the command.

### Step 2. Download UMP

Use the download button on the left at Bitbucket to download a zip archive of the UMP repo. Unpack the UMP files (`UltimateProxy.py`, `proxyfinder.py` and `proxyserver.py`, the rest can be discarded) into a directory of your choice.

### Step 3. Create a convenient launcher

Open *TextEdit* that is located in the Applications folder on your Mac.

Once the editor opens, type in the following:

```
#!/bin/bash
python /path/to/ump/directory/UltimateProxy.py
```

replacing `/path/to/ump/directory/` with the path to the directory you unpacked UMP into. 

Then select *Format > Convert To Plain Text (Cmd-Shift-T)* in the menu.

Save the file as something like `UMP.command`. Observe the file extension so that it would be `.command`, not `.txt` or whatever TextEdit might like.

Open *Terminal*, and type in `chmod +x `, then drag the file you just saved into the Terminal window. It's path should appear after `chmod +x `. Hit enter. The command should produce no output and return you to the prompt.


### If you've done it right...

Once you double-click the `UMP.command` file you've saved, a Terminal window should open. Shortly after, the UMP logo will appear in it. Then it will show you the port it's working on and a list of commands allowed. Stop UMP by typing Q, then pressing Enter.

### Finishing up

Refer to your media player's user manual to set the streaming proxy to HTTP type on address `127.0.0.1` port `3128`. **It is strongly *not* recommended to set UMP as the system proxy because it is not that stable yet.** Start UMP from the shortcut you've created before. Open a Digitally Imported stream in your player you've just configured (refer to DI, i.e. [here](http://pub7.di.fm), for a list of streams). Instead of a restriction notice, you should hear the current broadcast of the station you've chosen. You're now set up with UMP as a DI.fm proxy, enjoy!
