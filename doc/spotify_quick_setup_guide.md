# UMP Quick Setup Guide (with Spotify)

## On Windows

*Warning:* I've already had Python with PIP up and running so this is only correct to the point of how my memory keeps it :) refer to the official documentation in case you have any problems

### Step 0. Install Spotify

In case you need an older version, [FileHorse](http://www.filehorse.com/download-spotify/15011/old-versions/) seems to be a good choice to get an installer of one. However you will also need to prevent automatic updates, which will not be covered by this guide. There are plenty of guidelines on the web already.

### Step 1. Install Python 2.7 with PIP

You can get Python 2.7 on the [official website](https://www.python.org/downloads/), this should include PIP as well.

Run the installer and follow the instructions. The preferred installation directory is `C:\Python27` and it will be used in all further instructions. 

### Step 2. Set up required dependencies

So far the only dependency is BeautifulSoup. Install it with the following command: `C:\Python27\scripts\pip install beautifulsoup`

### Step 3. Download UMP

Use the download button on the left at Bitbucket to download a zip archive of the UMP repo. Unpack the UMP files (`UltimateProxy.py`, `proxyfinder.py` and `proxyserver.py`, the rest can be discarded) into your Spotify install directory (i.e. `C:\Users\%user_name%\AppData\Roaming\Spotify`). 

### Step 4. Modify the Spotify shortcut

Right-click the Spotify shortcut on your Desktop and select Properties. In the dialog that appears, change:

* Working directory: to your Python directory, `C:\Python27`
* Executable path: to a command line like `C:\Python27\python.exe UltimateProxy.py --enable-spotify --strict --execute Spotify.exe`

**Note:** In case you have a UK account, add `--united-kingdom` after `UltimateProxy.py`


Click Apply and then OK. Repeat the same process with your taskbar and Start menu shortcuts.

### If you've done it right...

Once you double-click the Spotify shortcut, a command prompt window should appear. Shortly after, the UMP logo will appear in it. Then it will take some time to find a proxy server and after it's done, Spotify should start.

**Note:** For good measure, never close the UMP window before closing Spotify. And once you close Spotify, the UMP window should close shortly after anyway.

### Finishing up

Edit the shortcuts to start UMP minimized. Configure Spotify to use an HTTP proxy at `127.0.0.1` port `3128`. You should now be good to go. Enjoy!

## On Mac

Macs usually come with Python preinstalled, but the rest is almost the same.

### Step 0. Install Spotify

In case you need an older version, [FileHorse](http://mac.filehorse.com/download-spotify/old-versions/) seems to be a good choice to get an installer of one. However you will also need to prevent automatic updates, which will not be covered by this guide. There are plenty of guidelines on the web already.

### Step 1. Install the dependencies

So far the only dependency is BeautifulSoup. Install it with the following command in Terminal: `pip install beautifulsoup`. Depending on your OS setup you might need administrative rights, in that case add `sudo` in front of the command.

### Step 2. Download UMP

Use the download button on the left at Bitbucket to download a zip archive of the UMP repo. Unpack the UMP files (`UltimateProxy.py`, `proxyfinder.py` and `proxyserver.py`, the rest can be discarded) into a directory of your choice.

### Step 3. Create a convenient launcher

Open *AppleScript Editor* that is located in the Utilities folder in the Applications folder on your Mac.

Once the editor opens, type in the following:

```
do shell script "python /path/to/ump/directory/UltimateProxy.py --enable-spotify --strict --execute \"/Applications/Spotify.app/Contents/MacOS/Spotify\" 2>/dev/null"
```

replacing `/path/to/ump/directory/` with the path to the directory you unpacked UMP into. 

**Note:** In case you have a UK account, add `--united-kingdom` after `UltimateProxy.py`

Save the script as an Application to a convenient location. Optionally assign an icon through the Get Info window in Finder.

### If you've done it right...

Once you open the Application you saved from AppleScript Editor, Spotify should open shortly after. Configure Spotify to use an HTTP proxy at `127.0.0.1` port `3128`. You should be all set.

**Note:** For good measure, never quit the shortcut application before closing Spotify. And once you close Spotify, the shortcut application should quit automatically shortly after.
